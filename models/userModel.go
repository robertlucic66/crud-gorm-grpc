package models

import "gorm.io/gorm"

type UserInfo struct {
	gorm.Model
	Name string
	Age  int32
}

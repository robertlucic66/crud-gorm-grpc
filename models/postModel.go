package models

import "gorm.io/gorm"

type PostInfo struct {
	gorm.Model
	Title        string
	Body         string
	UserInfoID   int
	UserInfo     UserInfo
	ReqIpAddress string
}

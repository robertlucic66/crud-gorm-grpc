package main

import (
	"context"
	"io"
	"log"
	"time"

	pb "gitlab.com/robertlucic66/crud-gorm-grpc/protos"
	"google.golang.org/grpc"
)

const (
	address = "localhost:3000"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	client := pb.NewUserClient(conn)

	// runDeleteUser(client, 4)
	// runCreateUser(client, "Fourth user", 33)
	// runUpdateUser(client, 2, "Second User", 24)
	// runGetUser(client, 1)
	// runGetUsers(client)

	// runCreatePost(client, "Sedmi post", "Body sedmog posta...", 1)
	// runGetPosts(client)
	runGetPostsOfUser(client, 5)

}

func runGetUsers(client pb.UserClient) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.Empty{}
	stream, err := client.GetUsers(ctx, req)
	if err != nil {
		log.Fatalf("%v.GetUsers(_) = _, %v", client, err)
	}
	for {
		row, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.GetUsers(_) = _, %v", client, err)
		}
		log.Printf("UserInfo: %v", row)
	}
}

func runGetUser(client pb.UserClient, userid int32) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.Id{Value: userid}
	res, err := client.GetUser(ctx, req)
	if err != nil {
		log.Fatalf("%v.GetUser(_) = _, %v", client, err)
	}
	log.Printf("UserInfo: %v", res)
}

func runCreateUser(client pb.UserClient, name string, age int32) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.UserInfo{Name: name, Age: age}
	res, err := client.CreateUser(ctx, req)
	if err != nil {
		log.Fatalf("%v.CreateUser(_) = _, %v", client, err)
	}
	if res.GetValue() != 0 {
		log.Printf("CreateUser Id: %v", res)
	} else {
		log.Printf("CreateUser Failed")
	}
}

func runUpdateUser(client pb.UserClient, userid int32, name string, age int32) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.UserInfo{Id: userid, Name: name, Age: age}
	res, err := client.Updateuser(ctx, req)
	if err != nil {
		log.Fatalf("%v.UpdateUser(_) = _, %v", client, err)
	}
	if int(res.GetValue()) == 1 {
		log.Printf("UpdateUser Succes")
	} else {
		log.Printf("UpdateUser Failed")
	}
}

func runDeleteUser(client pb.UserClient, userid int32) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.Id{Value: userid}
	res, err := client.DeleteUser(ctx, req)
	if err != nil {
		log.Fatalf("%v.Deleteuser(_) = _, %v", client, err)
	}
	if int(res.GetValue()) == 1 {
		log.Printf("DeleteUser Succes")
	} else {
		log.Printf("DeleteUser Failed")
	}
}

func runCreatePost(client pb.UserClient, title string, body string, userid int32) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.PostInfo{Title: title, Body: body, UserInfoID: userid, ReqIpAddress: runGetIp(client, ctx)}
	res, err := client.CreatePost(ctx, req)
	if err != nil {
		log.Fatalf("%v.CreatePost(_) = _ %v", client, err)
	}
	if res.GetValue() != 0 {
		log.Printf("CreatePost Id: %v", res)
	} else {
		log.Printf("CreatePost Failed")
	}
}

func runGetPosts(client pb.UserClient) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.Empty{}
	stream, err := client.GetPosts(ctx, req)
	if err != nil {
		log.Fatalf("%v.GetPosts(_) = _, %v", client, err)
	}
	for {
		row, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.GetPosts(_) = _, %v", client, err)
		}
		log.Printf("PostInfo: %v", row)
	}
}

func runGetPostsOfUser(client pb.UserClient, userid int32) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &pb.Id{Value: userid}
	stream, err := client.GetPostsOfUser(ctx, req)
	if err != nil {
		log.Fatalf("%v.GetPostsOfUser(_) = _, %v", client, err)
	}
	log.Printf("Posts of user ID = %v", userid)
	for {
		row, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.GetPostsOfUser(_) = _, %v", client, err)
		}
		log.Printf("PostInfo: %v", row)
	}

}

func runGetIp(client pb.UserClient, ctx context.Context) string {
	req := &pb.Empty{}
	res, err := client.GetIp(ctx, req)
	if err != nil {
		log.Fatalf("Could not get ip address")
	}
	log.Printf("runGetIp invoked: %v", res.Result)
	return res.Result
}

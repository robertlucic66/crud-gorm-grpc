package initdatabase

import (
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectToDB() {
	var err error
	dsn := "host=mouse.db.elephantsql.com user=crfzllou password=40dXIiJJyCLcjIEpZqYu9v4EJZLDTRNP dbname=crfzllou port=5432 sslmode=disable"
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to connect to database")
	}
}

package main

import (
	"gitlab.com/robertlucic66/crud-gorm-grpc/initdatabase"
	"gitlab.com/robertlucic66/crud-gorm-grpc/models"
)

func init() {
	initdatabase.ConnectToDB()
}

func main() {
	initdatabase.DB.AutoMigrate(&models.UserInfo{})
	initdatabase.DB.AutoMigrate(&models.PostInfo{})
}

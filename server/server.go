package main

import (
	"context"
	"log"
	"net"

	"gitlab.com/robertlucic66/crud-gorm-grpc/initdatabase"
	"gitlab.com/robertlucic66/crud-gorm-grpc/models"
	pb "gitlab.com/robertlucic66/crud-gorm-grpc/protos"
	"google.golang.org/grpc"
	"google.golang.org/grpc/peer"
)

const (
	port = ":3000"
)

type userServer struct {
	pb.UnimplementedUserServer
}

func init() {
	initdatabase.ConnectToDB()
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterUserServer(s, &userServer{})

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (s *userServer) GetUsers(in *pb.Empty,
	stream pb.User_GetUsersServer) error {
	log.Printf("Recieved: %v", in)

	var usersList []*pb.UserInfo
	initdatabase.DB.Find(&usersList)

	for _, user := range usersList {
		if err := stream.Send(user); err != nil {
			return err
		}
	}
	return nil
}

func (s *userServer) GetUser(ctx context.Context,
	in *pb.Id) (*pb.UserInfo, error) {
	log.Printf("Recieved: %v", in)

	var user *pb.UserInfo
	initdatabase.DB.First(&user, in.GetValue())

	return user, nil
}

func (s *userServer) CreateUser(ctx context.Context,
	in *pb.UserInfo) (*pb.Id, error) {
	log.Printf("Recieved: %v", in)

	user := &models.UserInfo{
		Name: in.Name,
		Age:  in.Age,
	}

	initdatabase.DB.Create(&user)
	res := pb.Id{}
	res.Value = int32(user.ID)

	return &res, nil
}

func (s *userServer) Updateuser(ctx context.Context,
	in *pb.UserInfo) (*pb.Status, error) {
	log.Printf("Recieved: %v", in)

	res := pb.Status{}

	var user *models.UserInfo
	initdatabase.DB.First(&user, in.Id)
	user.Name = in.Name
	user.Age = in.Age
	initdatabase.DB.Save(&user)
	res.Value = 1

	return &res, nil
}

func (s *userServer) DeleteUser(ctx context.Context,
	in *pb.Id) (*pb.Status, error) {
	log.Printf("Recieved: %v", in)

	res := pb.Status{}

	initdatabase.DB.Delete(&pb.UserInfo{}, in.GetValue())
	res.Value = 1

	return &res, nil
}

func (s *userServer) CreatePost(ctx context.Context,
	in *pb.PostInfo) (*pb.Id, error) {
	log.Printf("Recieved: %v", in)

	post := &models.PostInfo{
		Title:        in.Title,
		Body:         in.Body,
		UserInfoID:   int(in.UserInfoID),
		ReqIpAddress: in.ReqIpAddress,
	}

	initdatabase.DB.Create(post)
	res := pb.Id{}
	res.Value = int32(post.ID)

	return &res, nil
}

func (s *userServer) GetPosts(in *pb.Empty,
	stream pb.User_GetPostsServer) error {

	var postsList []*pb.PostInfo
	initdatabase.DB.Find(&postsList)

	for _, post := range postsList {
		if err := stream.Send(post); err != nil {
			return err
		}
	}

	return nil
}

func (s *userServer) GetPostsOfUser(in *pb.Id,
	stream pb.User_GetPostsOfUserServer) error {

	var postsList []*pb.PostInfo
	initdatabase.DB.Find(&postsList)

	for _, post := range postsList {
		if post.UserInfoID == in.Value {
			if err := stream.Send(post); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *userServer) GetIp(ctx context.Context, empty *pb.Empty) (*pb.IpAddress, error) {
	var address string
	p, ok := peer.FromContext(ctx)
	if ok {
		address = p.Addr.String()
	}
	res := &pb.IpAddress{
		Result: address,
	}

	log.Printf("Request from IP Address: %v", res.Result)

	return res, nil
}
